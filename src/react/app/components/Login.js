import React                        from 'react';
import { connect }                  from 'react-redux';
import { Redirect }                 from 'react-router-dom';

class Login extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        if(this.props.userInfo.id){
            return <Redirect to={{ pathname: '/' }} />
        }
        return(
            <p>Login</p>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        userInfo: state['edubig/app'].userInfo
	};
}
export default connect(_mapStateToPropsTop)(Login);
