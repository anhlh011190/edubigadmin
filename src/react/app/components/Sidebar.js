import React, { Component } from 'react';
import { Link }            	from 'react-router-dom';
import { connect }          from 'react-redux';

class Sidebar extends Component {
    constructor(props) {
        super(props);
    }

    activePage(page){

    }

    render() {
        let cssClass = "media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12";
        let cssClassActive = "media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12 active";
        return (
            <div id="sideNav" className="col-auto u-sidebar-navigation-v1 u-sidebar-navigation--dark">
                <div className="">
                    <div id="accordion" className="">
                        {
                            <div>
                                <div className="card1">
                                    <div className="">
                                        <Link onClick = {this.activePage.bind(this,"DASHBOARD")} className= {this.props.page == "DASHBOARD" ? cssClassActive :cssClass} to={'/'} >
                                            <span className="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
                                                <i className="hs-admin-server"></i>
                                            </span>
                                            <span className="media-body align-self-center">DASHBOARD</span>
                                            <span className="u-side-nav--has-sub-menu__indicator"></span>
                                        </Link>
                                    </div>
                                </div>

                                <div className="card1">
                                    <div className="">
                                        <a className="collapsed card-link media u-side-nav--top-level-menu-link u-side-nav--hide-on-hidden g-px-15 g-py-12" data-toggle="collapse" href="#collapseFour">
                                            <span className="d-flex align-self-center g-pos-rel g-font-size-18 g-mr-18">
                                                <i className="hs-admin-server"></i>
                                            </span>
                                            <span className="media-body align-self-center">TOOL</span>
                                        </a>
                                    </div>
                                    <div id="collapseFour" className={this.props.page == "CHECKLOG"||this.props.page == "LOGAPI" ? "collapse show": "collapse"} data-parent="#accordion">
                                        <Link onClick = {this.activePage.bind(this,"CHECKLOG")} className= {this.props.page == "CHECKLOG" ? cssClassActive :cssClass} to={'/checklog'} data-hssm-target="#subMenu1">
                                            <span className="media-body align-self-center">ABC</span>
                                            <span className="u-side-nav--has-sub-menu__indicator"></span>
                                    
                                            <span className="caret"></span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

function _mapStateToPropsTop(state) {
	return {
        userInfo: state['edubig/app'].userInfo
	};
}
export default connect(_mapStateToPropsTop)(Sidebar);