import React                        from 'react';
import { connect }                  from 'react-redux';
import { crawlerQuest }             from './../actions';

class Homepage extends React.Component {
    constructor(props){
        super(props);
    }

    handleCrawlerQuest = () => {
        this.props.dispatch(crawlerQuest());
    }

    render(){
        return(
            <div className="vcrx-qllh">
                <p>Trang quản trị</p>
                <p>Đang cập nhật...</p>
                <button onClick={this.handleCrawlerQuest} className="btn btn-xs btn-primary" type="submit">Crawler Quest</button>
            </div>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
	};
}
export default connect(_mapStateToPropsTop)(Homepage);
