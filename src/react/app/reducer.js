import { ReducerRegistry }  from '../base/redux';
let stateDefault = {
    userInfo: {
        id: 1
    }
}

ReducerRegistry.register('edubig/app', (state = stateDefault, action) => {
    switch (action.type) {
        default:
            return state;
    }
});