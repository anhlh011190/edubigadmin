import React                        from "react";
import { connect }                  from 'react-redux';
import { Redirect }                 from 'react-router-dom';
import { 
    Header, Sidebar
}                                   from "./../../../app";

class PrivateComponent extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        if(!this.props.userInfo.id){
            return <Redirect to={{ pathname: '/login' }} />
        }

        if ((this.props.roles && this.props.roles.indexOf(this.props.userInfo.role) === -1) && (this.props.userIds && this.props.userIds.indexOf(this.props.userInfo.id) === -1) ) {
            return <Redirect to={{ pathname: '/'}} />
        }

        return(
            <>
                <Header />
                <section className="container-fluid px-0 g-pt-65">
                    <div className="row no-gutters g-pos-rel g-overflow-x-hidden">
                        <Sidebar />
                        {this.props.children}
                    </div>
                </section>
            </>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        userInfo: state['edubig/app'].userInfo
	};
}
export default connect(_mapStateToPropsTop)(PrivateComponent);
