import React        from 'react';
import ReactDOM     from 'react-dom';
import { Provider }         from 'react-redux';
import { createStore }      from 'redux';
import Thunk                from 'redux-thunk';
import {
    Login, Homepage, Admin
}                           from './react/app';
import {
    PrivateComponent
}                           from './react/base/author';
import {
    BrowserRouter as Router,
    Route
}                           from 'react-router-dom';
import { browserHistory }   from 'react-dom';
import * as serviceWorker   from './serviceWorker';
import { 
    StateListenerRegistry, 
    ReducerRegistry, 
    MiddlewareRegistry 
}                           from './react/base/redux';

window.APP = {}

let _createStore = () => {
    let middleware = MiddlewareRegistry.applyMiddleware(Thunk);
    const reducer = ReducerRegistry.combineReducers();
    const store = createStore( reducer, middleware );
    StateListenerRegistry.subscribe(store);
    if (typeof APP !== 'undefined') {
        window.APP.store = store;
    }
    return store;
}
let store = _createStore();

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <PrivateComponent>
                <Route exact path="/"               component={Homepage}/>
            </PrivateComponent>
            <PrivateComponent roles={[2]} userIds={[123]}>
                <Route exact path="/admin"          component={Admin}/>
            </PrivateComponent>
            <Route exact path="/login"              component={Login}/>
        </Router>
    </Provider>
    , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
